<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitf326f0c0ad0ce06f4395cd3f8d25d236
{
    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInitf326f0c0ad0ce06f4395cd3f8d25d236::$classMap;

        }, null, ClassLoader::class);
    }
}
