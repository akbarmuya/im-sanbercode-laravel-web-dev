@extends('layout.master')
@section('title')
    Halaman Tampil Category
@endsection
@section('content')
    
  <a href="/category/create"  class="btn btn-primary btn-sm my-2">Tambah</a>
  <table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Name</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>

        @forelse ($categories as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->name}}</td>
                <td>
                    
                    <form action="/category/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                     <a href="/category/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/category/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                       
                        <input type="submit" value="delete" class=" btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td> Data Category Kosong</td>
            </tr>
        @endforelse

    </tbody>
  </table>
@endsection

