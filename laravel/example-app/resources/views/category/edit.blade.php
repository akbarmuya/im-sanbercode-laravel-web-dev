@extends('layout.master')
@section('title')
    Halaman Tampil Edit Category
@endsection
@section('content')

@extends('layout.master')
@section('title')

Halaman Tambah Kategori
@endsection
@section('content')
<form method="POST" action="/category/{{$category->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Category Name</label>
      <input type="text" value="{{$category->name}}" name="name" class="form-control">
    </div>

    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Category Description</label>
      <textarea name="description"  class="form-control" cols="30" rows="10">{{$category->description}}</textarea>
    </div>

    @error('description')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection




@endsection