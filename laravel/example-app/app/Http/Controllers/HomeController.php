<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function utama()
{
    return view('welcome');
}


    public function bio()
{
    return view('halaman.biodata');
}

    public function kirim(Request $request)
{
    $firstname = $request['firstname'];
    $lastname = $request['lastname'];
    $gender = $request['gender'];
    $nationality = $request['natioanlity'];
    $languagespoken= $request['firstname'];
    $bio = $request['bio'];

    return view('halaman.terimakasih', ['firstname' => $firstname, 'lastname' => $lastname, 'gender' => $gender, 'nationality' => $nationality, 'languagespoken' => $languagespoken, 'bio' => $bio]);
}
}