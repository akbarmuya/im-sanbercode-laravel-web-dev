<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CategoriesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

Route::get('/biodata', [HomeController::class, 'bio']);

Route::post('/kirim', [HomeController::class, 'kirim']);

Route::get('/data-tables', function(){
    return view('pages.datatable');
});
Route::get('/table', function(){
    return view('pages.table');
});

//CRUD

//CREATE DATA

//route untuk mengarah ke form tambah category
Route::get('/category/create', [CategoriesController::class, 'create']);
//route untuk menyimpan data inputan ke DB table category
Route::post('/category',[CategoriesController::class, 'store']);

//read data
//route untuk menampilkan semua data di table data kategori database
Route::get('/category',[CategoriesController::class, 'index']);
//route untuk ambil data bedasarkan id
Route::get('/category/{id}',[CategoriesController::class, 'show']);
//update data
//route untuk mengarah ke form edit kategori dengan membawa data berdasrka id
Route::get('/category/{id}/edit',[CategoriesController::class, 'edit']);
//route  untuk update berdasarkan id
Route::put('/category/{id}',[CategoriesController::class, 'update']);
//Delete data
//route untuk hapus data berdasarkan id categorynya
Route::delete('/category/{id}',[CategoriesController::class, 'destroy']);
