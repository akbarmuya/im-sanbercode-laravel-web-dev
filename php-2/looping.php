<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>LOOPING</title>
</head>
<body>
    <h1>Contoh Looping</h1>
    <?php
    echo "<h3>Contoh soal 1</h3>";
    echo "<h4>Looping 1</h4>";

    for($i=2; $i<=20; $i+=2){
        echo "$i - I Love PHP <br>";
    }
    
    echo "<h4>Looping 2</h4>";
    
    $a = 20;
    while($a>=1){
        echo "$a - I Love PHP <br>";
        $a -= 2;
    }

    echo "<h3>Contoh soal 2</h3>";

    $nomor =  [18, 45, 29, 61, 47, 34];

    echo "array Numbers : " ;
    print_r($nomor);

    foreach($nomor as $value){
        $rest[] = $value %= 4;
    }
    echo "<br>";
    echo "array sisa bagi 4 adalah: " ;
    print_r($rest);

    echo "<h3>Contoh soal 3</h3>";
        
        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];

        //   Output: 

          foreach($items as $values){
            $tampung = [
                "id" => $values[0],
                "name" => $values[1],
                "price" => $values[2],
                "description" => $values[3],
                "source" => $values[4]
            ];
            print_r($tampung);
            echo "<br>";
        }
    echo "<h3>Contoh soal 4</h3>";

    for($j=1; $j<=5; $j++){
        for($k=$j; $k<=5; $k++){
            echo $k;
        }
        echo "<br>";
    }

    ?>
</body>
</html>